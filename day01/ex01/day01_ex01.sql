SELECT object_name 
FROM ( 
    SELECT name AS object_name, 1 AS ordering 
    FROM person 
    UNION ALL 
    SELECT pizza_name AS object_name, 2 AS ordering 
    FROM menu 
) AS subquery 
ORDER BY ordering, object_name;