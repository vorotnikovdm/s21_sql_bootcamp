SELECT subquery.action_date, person.name as person_name
FROM ( 
    SELECT order_date AS action_date, person_id
    FROM person_order 
 
    INTERSECT 
 
    SELECT visit_date AS action_date, person_id
    FROM person_visits 
) AS subquery 
JOIN person ON subquery.person_id = person.id
ORDER BY action_date ASC, person_name DESC;