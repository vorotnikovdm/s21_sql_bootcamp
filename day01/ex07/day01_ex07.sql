SELECT subquery.order_date, CONCAT (person.name, ' (age:', person.age,')') as person_information
FROM (
	SELECT order_date, person_id
	FROM person_order
) AS subquery
JOIN person ON subquery.person_id = person.id
ORDER BY order_date, person_information;