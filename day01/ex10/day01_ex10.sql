SELECT person.name AS person_name, menu.pizza_name, pizzeria.name as pizzeria_name
FROM person_order
JOIN person ON person_order.person_id = person.id
JOIN menu ON person_order.menu_id = menu.id
JOIN pizzeria ON pizzeria.id = menu.pizzeria_id
ORDER BY person_name ASC, pizza_name ASC, pizzeria_name ASC;