SELECT generate_series::date AS missing_date
FROM generate_series('2022-01-01'::date, '2022-01-10'::date, '1 day')
LEFT JOIN (
	SELECT DISTINCT visit_date
	FROM person_visits
	WHERE person_id = 1 OR person_id = 2
) AS visited_dates ON generate_series::date = visited_dates.visit_date
WHERE visited_dates.visit_date IS NULL
ORDER BY missing_date ASC;