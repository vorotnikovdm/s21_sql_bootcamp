SELECT 
	COALESCE (person.name, '-') as person_name, 
	visit_date, 
	COALESCE (pizzeria.name, '-') as pizzeria_name
FROM (
	SELECT *
	FROM person_visits
	WHERE visit_date BETWEEN '2022-01-01' AND '2022-01-03'
) as subquery
FULL JOIN person ON person.id = subquery.person_id
FULL JOIN pizzeria ON pizzeria.id = subquery.pizzeria_id
ORDER by person_name, visit_date, pizzeria_name;