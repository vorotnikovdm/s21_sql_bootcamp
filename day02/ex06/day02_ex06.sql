SELECT pizza_name, pizzeria.name as pizzeria_name
FROM menu
JOIN person_order ON person_order.menu_id = menu.id
JOIN person ON person.id = person_order.person_id
JOIN pizzeria ON pizzeria.id = menu.pizzeria_id
WHERE person.name IN ('Denis' , 'Anna')
ORDER BY menu.pizza_name, pizzeria.name;