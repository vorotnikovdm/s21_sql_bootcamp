SELECT pizzeria.name AS pizzeria_name 
FROM person_visits 
JOIN pizzeria ON person_visits.pizzeria_id = pizzeria.id 
JOIN person ON person_visits.person_id = person.id 
LEFT JOIN ( 
    SELECT DISTINCT menu.pizzeria_id 
    FROM menu
    JOIN person_order ON person_order.menu_id = menu.id 
    JOIN person ON person.id = person_order.person_id 
    WHERE person.name = 'Andrey' 
) AS ordered_pizzerias ON pizzeria.id = ordered_pizzerias.pizzeria_id 
WHERE person.name = 'Andrey' AND ordered_pizzerias.pizzeria_id IS NULL
ORDER BY pizzeria_name;