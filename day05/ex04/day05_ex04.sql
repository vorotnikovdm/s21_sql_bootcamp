DROP INDEX IF EXISTS idx_menu_unique;
CREATE UNIQUE INDEX idx_menu_unique ON menu (pizzeria_id, pizza_name);
SET enable_seqscan TO OFF;

EXPLAIN ANALYZE
SELECT pizza_name, pizzeria_id
FROM menu
WHERE pizza_name = 'cheese pizza' AND pizzeria_id = 1;