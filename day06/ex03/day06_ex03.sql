DROP INDEX IF EXISTS idx_person_discounts_unique;
CREATE UNIQUE INDEX idx_person_discounts_unique ON person_discounts (person_id, pizzeria_id);
SET enable_seqscan TO OFF;

EXPLAIN ANALYZE
SELECT person_id, pizzeria_id
FROM person_discounts
WHERE person_id = 1 AND pizzeria_id = 1;