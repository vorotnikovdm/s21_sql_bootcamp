COMMENT ON TABLE person_discounts IS 'This table stores personal discount at different pizzerias';
COMMENT ON COLUMN person_discounts.id IS 'Unique id for each record in the table';
COMMENT ON COLUMN person_discounts.person_id IS 'Unique id for each person that got a discount';
COMMENT ON COLUMN person_discounts.pizzeria_id IS 'Unique id for pizzeria which discount is for';
COMMENT ON COLUMN person_discounts.discount IS 'Percent of discount for each person at a specific pizzeria';