SELECT *
FROM (SELECT pizzeria.name, COUNT(person_order.person_id) as count, 'order' as action_type
FROM person_order
JOIN menu ON person_order.menu_id = menu.id
JOIN pizzeria ON menu.pizzeria_id = pizzeria.id
GROUP BY pizzeria.name
ORDER BY action_type ASC, COUNT DESC
LIMIT 3) as suborder

UNION

SELECT *
FROM (SELECT pizzeria.name, COUNT(person_visits.person_id) as count, 'visit' as action_type
FROM person_visits
JOIN pizzeria ON pizzeria.id = person_visits.pizzeria_id
GROUP BY pizzeria.name
ORDER BY action_type ASC, COUNT DESC
LIMIT 3) as subvisit
ORDER BY action_type ASC, COUNT DESC;