WITH 
cte_order AS (
SELECT pizzeria.name, COUNT(person_order.person_id) as count, 'order' as action_type
FROM person_order
JOIN menu ON person_order.menu_id = menu.id
JOIN pizzeria ON menu.pizzeria_id = pizzeria.id
GROUP BY pizzeria.name),

cte_visit AS 
(SELECT pizzeria.name, COUNT(person_visits.person_id) as count, 'visit' as action_type
FROM person_visits
JOIN pizzeria ON pizzeria.id = person_visits.pizzeria_id
GROUP BY pizzeria.name)

SELECT pizzeria.name, COALESCE(cte_order.count, 0) + COALESCE(cte_visit.count, 0) as total_count
FROM pizzeria
FULL JOIN cte_order ON cte_order.name = pizzeria.name
JOIN cte_visit ON cte_visit.name = pizzeria.name
ORDER BY total_count DESC, name ASC;