WITH cte_comparison AS (
SELECT 
	address,
	ROUND((MAX(age::numeric) - (MIN(age::numeric) / MAX(age::numeric))), 2) as formula,
	ROUND(AVG(age::numeric), 2) as average
FROM person
GROUP BY address)

SELECT 
	address,
	formula,
	average,
	CASE
		WHEN formula > average
		THEN true
		ELSE false
	END as comparison
FROM cte_comparison
ORDER BY cte_comparison.address;