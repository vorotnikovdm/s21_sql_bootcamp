DROP FUNCTION IF EXISTS fnc_persons_female;
DROP FUNCTION IF EXISTS fnc_persons_male;

CREATE OR REPLACE FUNCTION fnc_persons(IN pgender VARCHAR DEFAULT 'female')
RETURNS TABLE (id BIGINT, name VARCHAR, age INT, gender VARCHAR, address VARCHAR) 
AS $$
	SELECT id, name, age, gender, address
	FROM person
	WHERE gender = pgender;
$$ LANGUAGE sql;