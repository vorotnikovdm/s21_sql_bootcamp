CREATE OR REPLACE FUNCTION func_minimum(VARIADIC arr NUMERIC[])
RETURNS NUMERIC AS $$
DECLARE
	min_val NUMERIC;
BEGIN
	min_val = (SELECT MIN(i) FROM unnest(arr) i);
	RETURN min_val;
END;
$$ LANGUAGE plpgsql;

SELECT func_minimum(VARIADIC arr => ARRAY[10.0, -1.0, 5.0, 4.4]);