CREATE OR REPLACE FUNCTION fnc_fibonacci(pstop INTEGER DEFAULT 10) 
RETURNS TABLE (fibonacci_number INTEGER) 
AS $$ 
DECLARE 
    a INTEGER := 0; 
    b INTEGER := 1; 
    temp INTEGER; 
BEGIN 
    fibonacci_number := a; 
    IF a < pstop THEN 
        RETURN NEXT; 
    END IF; 
 
    fibonacci_number := b; 
    IF b < pstop THEN 
        RETURN NEXT; 
    END IF; 
 
    WHILE TRUE LOOP 
        temp := a + b; 
        IF temp >= pstop THEN 
            RETURN; 
        END IF; 
        fibonacci_number := temp; 
        RETURN NEXT; 
        a := b; 
        b := temp; 
    END LOOP; 
END; 
$$ LANGUAGE plpgsql;