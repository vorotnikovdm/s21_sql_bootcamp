DROP TABLE IF EXISTS nodes;

CREATE TABLE nodes
(
    id SERIAL PRIMARY KEY,
    point1 varchar not null,
    point2 varchar not null,
    cost int not null
);

INSERT INTO nodes(point1, point2, cost)
VALUES ('a', 'b', 10),
       ('a', 'c', 15),
       ('a', 'd', 20),
       ('b', 'a', 10),
       ('b', 'c', 35),
       ('b', 'd', 25),
       ('c', 'a', 15),
       ('c', 'b', 35),
       ('c', 'd', 30),
       ('d', 'a', 20),
       ('d', 'b', 25),
       ('d', 'c', 30);
	   
WITH RECURSIVE tours AS (
	SELECT
		point1 AS start_point,
		point2 AS end_point,
		cost AS total_cost,
		ARRAY[point1, point2] AS tour,
		1 AS city_count
	FROM nodes
	WHERE point1 = 'a'
	
	UNION ALL
	
	SELECT
	tours.start_point,
	nodes.point2 AS end_point,
	tours.total_cost + nodes.cost AS total_cost,
	tours.tour || nodes.point2 AS tour,
	tours.city_count + 1 AS city_count
	FROM tours
	JOIN nodes ON tours.end_point = nodes.point1
	WHERE (tours.city_count < 4 AND nodes.point2 != ALL(tours.tour)) 
       OR (tours.city_count = 3 AND nodes.point2 = 'a')
)

SELECT total_cost, tour
FROM (
	SELECT 
		total_cost,
		tour,
		DENSE_RANK () OVER (ORDER BY total_cost) AS cost_rank
	FROM tours
	WHERE city_count = 4
) subquery
WHERE cost_rank = 1
ORDER BY total_cost, tour;