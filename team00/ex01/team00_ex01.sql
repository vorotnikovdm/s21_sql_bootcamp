WITH RECURSIVE tours AS (
	SELECT
		point1 AS start_point,
		point2 AS end_point,
		cost AS total_cost,
		ARRAY[point1, point2] AS tour,
		1 AS city_count
	FROM nodes
	WHERE point1 = 'a'
	
	UNION ALL
	
	SELECT
	tours.start_point,
	nodes.point2 AS end_point,
	tours.total_cost + nodes.cost AS total_cost,
	tours.tour || nodes.point2 AS tour,
	tours.city_count + 1 AS city_count
	FROM tours
	JOIN nodes ON tours.end_point = nodes.point1
	WHERE (tours.city_count < 4 AND nodes.point2 != ALL(tours.tour)) 
       OR (tours.city_count = 3 AND nodes.point2 = 'a')
)

SELECT total_cost, tour
FROM (
	SELECT 
		total_cost,
		tour,
		DENSE_RANK () OVER (ORDER BY total_cost) AS cost_rank,
		DENSE_RANK () OVER (ORDER BY total_cost DESC) AS cost_rank_desc
	FROM tours
	WHERE city_count = 4
) subquery
WHERE cost_rank = 1 OR cost_rank_desc = 1
ORDER BY total_cost, tour;