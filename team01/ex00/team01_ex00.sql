WITH cte_userbalance AS (
	SELECT
		"user".id as id,
		COALESCE ("user".name, 'not defined') as name,
		COALESCE ("user".lastname, 'not defined') as lastname,
		balance.type,
		balance.money,
		balance.currency_id
	FROM "user"
	RIGHT JOIN balance ON balance.user_id = "user".id
),
cte_currencydata AS (
	SELECT 
	    currency.id, 
 	    COALESCE(currency.name, 'not defined') AS currency_name, 
  	    COALESCE (MAX(currency.rate_to_usd), 1) AS rate_to_usd 
	FROM 
 	   currency 
	GROUP BY 
  	  currency.id, 
  	  currency.name
)
SELECT
	cte_userbalance.name,
	cte_userbalance.lastname,
	cte_userbalance.type,
	SUM(cte_userbalance.money) AS volume,
	COALESCE (cte_currencydata.currency_name, 'not defined'),
	COALESCE (cte_currencydata.rate_to_usd, 1) as last_rate_to_usd,
	SUM(cte_userbalance.money) * COALESCE(cte_currencydata.rate_to_usd, 1) AS total_volume_in_usd
FROM cte_userbalance
LEFT JOIN cte_currencydata ON cte_currencydata.id = cte_userbalance.currency_id
GROUP BY 
	cte_userbalance.name, 
	cte_userbalance.lastname, 
	cte_userbalance.type,
	cte_currencydata.currency_name,
	cte_currencydata.rate_to_usd
ORDER BY
	cte_userbalance.name DESC,
	cte_userbalance.lastname ASC,
	cte_userbalance.type ASC;