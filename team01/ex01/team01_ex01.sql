
WITH bal_id AS (
	SELECT ROW_NUMBER() OVER (ORDER BY (SELECT NULL)) as id, * 
FROM balance),
be as (
	SELECT bal_id.id, name, rate_to_usd
	FROM bal_id 
	join currency  as co on currency_id=co.id
	where co.updated  = 
	(select updated from currency 
		  where currency.updated < bal_id.updated 
		  and currency.id=currency_id
		  order by 1 desc limit 1 )),
af as (
	select bal_id.id, name, rate_to_usd
	from bal_id
	join currency as co on currency_id=co.id	
	where co.updated  = 
	(select updated from currency 
		  where currency.updated > bal_id.updated 
		  and currency.id=currency_id
		  order by 1 asc limit 1 )),
prep as (
	select be.*from be
	union 
	select af.* from af where af.id not in (select id from be))
select COALESCE(u.name, 'not defined') as name, 
		COALESCE(u.lastname, 'not defined') as lastname,
		prep.name as currency_name,
		rate_to_usd*money as currency_in_usd
		from prep
left join bal_id on prep.id=bal_id.id
left join "user" as u on u.id=user_id
order by 1 desc,2,3
-- select * from bal_id
		  
